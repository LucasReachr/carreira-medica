import {Component, HostListener, OnInit} from '@angular/core';
import {WordpressService} from '../../service/wordpress.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {

  CAROUSEL_BREAKPOINT = 768;
  slides: any = [[]];
  carouselDisplayMode = 'multiple';
  hoverImg: 'Teste';
  smallHoverImg = '<img src="//placehold.it/100x50"/>';

  cards = [
    {
      img: 'assets/logo-search.svg',
      descricao: 'Consultoria em recrutamento de executivos de alta e média gerência, assim como de especialistas com sólida atuação na área médica.',
      InternalId: 'http://www.searchrh.com.br/'
    },
    {
      img: 'assets/logo-reachr.svg',
      descricao: 'Plataforma de Recrutamento Digital com inteligência artificial.',
      InternalId: 'https://www.reachr.com.br/#/'
    },
    {
      img: 'assets/logotipo-DOM.svg',
      descricao: 'Software de Avaliação Comportamental',
      InternalId: 'http://www.dombrasil.net.br/'
    },
    {
      img: 'assets/logo-w1.jpg',
      descricao: 'Consultoria Financeira focada em otimizar sua vida financeira. Com padrão internacional de planejamento financeiro, traz soluções  financeiras de forma eficaz, pensando no seu futuro e da sua família.',
      InternalId: 'http://w1consultoria.com.br/'
    },
    {
      img: 'assets/logo-neo.jpeg',
      descricao: 'Oferecemos espaços físicos e virtuais (on demand), teleatendimentos, Produtos e Serviços com benefícios (parcerias e descontos), segurança e pagamento facilitado.',
      InternalId: 'https://neoo.work/'
    },
    {
      img: 'assets/santa-clara.png',
      descricao: 'Empresa especializada na prevenção à saúde de recém-nascidos, crianças, adolescentes, adultos e idosos. Mais de 25 unidades no Brasil e com especialização em atendimento corporativo. ',
      InternalId: 'https://santaclaravacinas.com.br/'
    },
    {
      img: 'assets/logo-olhos-limongi.png',
      descricao: 'O Hospital de Olhos Limongi é um espaço destinado para a saúde ocular, e tudo foi pensado para oferecer o que há de melhor em atendimento, conforto e segurança.',
      InternalId: 'http://www.hospitaldeolhoslimongi.com.br'
    },
    {
      img: 'assets/saudemaisacao2020.png',
      descricao: 'Empresa criada por médicos com o objetivo de auxiliar todos os profissionais da saúde a cuidarem adequadamente de suas finanças pessoais e das finanças de seus consultórios.',
      InternalId: 'https://saudemaisacao.com.br/'
    },
    {
      img: 'assets/donas-grana.jpg',
      descricao: 'Empresa Donas da grana',
      InternalId: 'https://donasdagrana.com.br/'
    },
  ];
  constructor(private wp: WordpressService, ) {}

  ngOnInit(): void {
    this.slides = this.chunk(this.cards, 4);

    if (window.innerWidth <= this.CAROUSEL_BREAKPOINT) {
      this.carouselDisplayMode = 'single';
    } else {
      this.carouselDisplayMode = 'multiple';
    }
  }

  irParaMentoriaIndividual(): void {
    window.open('https://www.carreiramedica.com.br/mentoria-individual/', '_blank');
  }

  irParaMentoriaEmGrupo(): void {
    window.open('https://www.carreiramedica.com.br/mentoria-em-grupo/', '_blank');
  }


  irParaDuvulgarVagas(): void {
    window.open('https://www.carreiramedica.com.br/divulgar-vagas/', '_blank');
  }

  chunk(arr: any, chunkSize: any): any {
    const R = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    return R;
  }

  @HostListener('window:resize')
  onWindowResize(): any {
    if (window.innerWidth <= this.CAROUSEL_BREAKPOINT) {
      this.carouselDisplayMode = 'single';
    } else {
      this.carouselDisplayMode = 'multiple';
    }
  }

}
