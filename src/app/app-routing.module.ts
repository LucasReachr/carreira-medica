import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from '../components/home/home.component';
import {FooterComponent} from '../components/footer/footer.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'fale-conosco', component: FooterComponent },
  {path: '', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [
    RouterModule.forRoot([
        { path: 'fale-conosco', component: FooterComponent }],
      { anchorScrolling: 'enabled', useHash: true })
  ],
  exports: [RouterModule]
})


export class AppRoutingModule { }
