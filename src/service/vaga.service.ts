import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VagaService {

  constructor(public http: HttpClient) { }

  getVagas(empresaId) {
    return this.http
      .get<Object[]>('api/VagaParts/GetVagasEmpresaCanais/' + empresaId);
  }
}
