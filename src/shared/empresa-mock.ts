export const empresaMock = {
  // id: '5b0d503a-19e1-42cb-84fd-90819faeb1bf',    // vagas da dexpertio
  id: '7ca777ef-1bdc-4ee5-acef-528e57508e95',       // vagas da clinica fares
  // id: '53b591a5-b118-4ac7-b555-ade6716bb6ec',    // vagas da autoavaliar
  title: 'Desperte o expert em você',
  google_id: 'UA-141862839-37',
  nome: 'Dexpertio',
  subtitulo: '<b>Consultoria</b> em serviços de <b>Atenção<br>Primária</b> centrada nos <b>seus desafios.</b>',
  url_site: 'https://dexpertio.com.br/',
  url_facebook: 'https://www.facebook.com/dexpertio/',
  url_linkedin: 'https://www.linkedin.com/company/dexpertio',
  url_instagram: 'https://www.instagram.com/dexpertio/',
  url_logo: 'assets/img/logo-dexpertio.png',
  url_banco_talentos: 'https://www.reachr.com.br/#/vaga/dexpertio/banco-de-talentos/7135/belo-horizonte/mg',
  url_blog_posts: 'https://www.reachr.com.br/novo-blog/wp-json/wp/v2/posts?_embed',
  social_icons_color: 'text-dark',
  banner_text_color: 'text-white',
  vagas_internas: true,      // chave para mostrar ou não o botão das vagas internas
  depoimentos: true,        // chave para mostrar ou não os depoimentos
  slideshow: true,          // chave para mostrar ou não o slideshow
  banner: true,              // chave para mostrar ou não a imagem do banner
  banner_conteudo: true,    // chave para mostrar ou não o texto com banner
  slideshow_conteudo: true,    // chave para mostrar ou não o texto com slideshow, combina os valores do slideshow com o banner_conteudo
};
