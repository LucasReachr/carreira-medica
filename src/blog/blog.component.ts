import { Component, OnInit } from '@angular/core';
import {WordpressService} from '../service/wordpress.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
  providers: [ WordpressService ]
})
export class BlogComponent implements OnInit {
  posts: Array<object>;
  loading: boolean;

  constructor(private wp: WordpressService, private router: Router) {
    this.wp.getPosts().subscribe((post: Array<object>) => {
      this.posts = post;
      console.log(this.posts);
    });
  }

  ngOnInit(): void {
  }

  irParaOBlog(): void {
    window.open('http://reachr.com.br/blog', '_blank');
  }

}
